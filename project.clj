(defproject zefix-clj "0.1.0"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [enlive "1.1.1"]
                 [clj-http "0.9.0"]
                 [org.clojure/data.xml "0.0.7"]
                 [enlive "1.1.4"]
                 [cheshire "5.2.0"]
                 [org.clojure/tools.trace "0.7.5"]
                 [com.taoensso/timbre "3.1.2"]
                 [org.clojure/data.zip "0.1.1"]
                 [com.datomic/datomic-free "0.9.4609"]]
  :repl-options {:init-ns zefix-clj.core})
