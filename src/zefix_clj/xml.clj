(ns zefix-clj.xml
  (:require [clojure.zip :as zip]
            [zefix-clj.scrape :refer [file->data]])
  (:use [clojure.data.zip.xml]))

(defn data-from-xml [xml]
  (let [zipped (zip/xml-zip xml)
        rubrics (xml1-> zipped :instances :instance :rubrics)]
    {:company-name (xml1-> rubrics :names :name text)
     :address (let [addr (xml1-> rubrics :addresses :address :addressDetails)]
                {:street (xml1-> addr :street text)
                 :nr (xml1-> addr :buildingNum text)
                 :zip (xml1-> addr :zip text)
                 :city (xml1-> addr :city text)})
     :purpose (xml1-> rubrics :purposes :purpose text)
     :capital (when-let [capital (xml1-> rubrics :capital :capitalItem)]
                {:currency (xml1-> capital (attr :currency))
                 :amount (xml1-> capital :capitalAmount text)
                 :shares (when-let [quantity (xml1-> capital :division :details :quantity text)]
                           {:quantity quantity
                            :code (xml1-> capital :division :details :shareCode text)
                            :type (xml1-> capital :division :details :shareText text)
                            :value (xml1-> capital :division :details :valueOfShare text)})})
     }))

(defn file->data-from-xml [file]
  (data-from-xml (file->data file)))

(defn files-from-dir [dir]
  (rest (file-seq (clojure.java.io/file dir))))
