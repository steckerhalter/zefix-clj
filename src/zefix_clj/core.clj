(ns zefix-clj.core
  (:use [zefix-clj.xml]
        [zefix-clj.scrape])
  (:require [clj-http.client :as client]
            [clojure.pprint :as pp]
            [clojure.tools.trace :as trace]
            [taoensso.timbre :as timbre]
            [datomic.api :as d])
  (:import [java.net]))

;; --- configure logging -----------------------------------------------------

(timbre/refer-timbre)
(timbre/set-config! [:appenders :standard-out :enabled?] false)
(timbre/set-config! [:appenders :spit :enabled?] true)
(timbre/set-config! [:shared-appender-config :spit-filename] "zefix-clj.log")

;; --- monkey patching `clj-http.util' to use ISO-8859-1 encoding ------------
;; see issue: https://github.com/dakrone/clj-http/issues/192

(in-ns 'clj-http.util)

(defn url-encode
  "Returns an UTF-8 URL encoded version of the given string."
  [unencoded]
  (URLEncoder/encode unencoded "ISO-8859-1"))

(in-ns 'zefix-clj.core)

;; ---------------------------------------------------------------------------

;; (defn capital [item]
;;   (-> item :capital :amount (as-> % (when % (read-string %)))))

;; (def items (map file->data-from-xml (files-from-dir "data/chnrs")))

;; (sort #(compare (capital %2) (capital %1)) items)
