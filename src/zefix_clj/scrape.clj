(ns zefix-clj.scrape
  (:require [clj-http.client :as client]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [clojure.xml :as xml]
            [clojure.data.xml :refer [parse-str]]
            [clojure.set :as set]
            [clojure.pprint :as pp]
            [net.cgrand.enlive-html :as html]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(def cantons
  {:ag {:name "Aargau" :id 400}
   :ai {:name "Appenzell Innerrhoden" :id 310}
   :ar {:name "Appenzell Ausserrhoden" :id 300}
   :be {:name "Bern" :id 36}
   :bl {:name "Basel-Landschaft" :id 280}
   :bs {:name "Basel-Stadt" :id 270}
   ;; fribourg has a different API
   ;; :fr {:name "Freiburg" :id 217}
   ;; genf has a different API
   ;; :ge {:name "Genf" :id 660}
   :gl {:name "Glarus" :id 160}
   :gr {:name "Graubünden" :id 350}
   :ju {:name "Jura" :id 670}
   :lu {:name "Luzern" :id 100}
   :ne {:name "Neuenburg" :id 645}
   :nw {:name "Nidwalden" :id 150}
   :ow {:name "Obwalden" :id 140}
   :sg {:name "St. Gallen" :id 320}
   :sh {:name "Schaffhausen" :id 290}
   :so {:name "Solothurn" :id 241}
   :sz {:name "Schwyz" :id 130}
   :tg {:name "Thurgau" :id 440}
   :ti {:name "Tessin" :id 501}
   :ur {:name "Uri" :id 120}
   ;; waadt has a different API
   ;; :vd {:name "Waadt" :id 550}
   :vso {:name "Oberwallis" :id 600}
   :vsu {:name "Unterwallis" :id 621}
   :vsz {:name "Zentralwallis" :id 626}
   :zg {:name "Zug" :id 170}
   :zh {:name "Zürich" :id 20}})

(defn get-chnrs

  "Get all CH-numbers from a specific CANTON and COMMUNITY.
CANTON is an abbreviation, e.g. `zh' (string).
COMMUNITY is the legal municipality, e.g. `Dietikon' (string)."

  [canton community]

  (let [amt (str (:id ((keyword canton) cantons)))
        data (parse-str
              (:body
               ;; we use the zh subdomain here, it doesn't matter though
               (client/post (str "http://zh.powernet.ch/webservices/net/zefix/zefix.asmx/SearchDiv")
                            {:form-params
                             {:key community
                              :suchen_in "000*Sitze/Sitz"
                              :xml "on"
                              :rf ""
                              :suffix "on"
                              :suche_nache "aktuell"
                              :suche_nach "aktuell"
                              :pers_sort "original"
                              :alle_eintr "true"
                              :amt amt
                              :language "1"
                              :hrg_opt "010100"
                              :header "1"}})))]

    (map #(first (:content %)) (:content data))))

(defn data-file-name [canton community]
  (format "data/%s-%s.clj" canton (string/replace community " " "_")))

(defn data->file "Print data to file.
If :pretty arg is non-nil, indent the representation."
  [data file-name & {:keys [pretty] :or {pretty nil}}]
  (let [func (if pretty pp/pprint pr)
        file (io/as-file file-name)]
    (-> file
        (.getAbsoluteFile)
        (.getParentFile)
        (.mkdirs))
    (with-open [w (io/writer file-name)]
      (binding [*out* w]
        (func data)))))

(defn file->data [file-name]
  (with-open [r (java.io.PushbackReader. (io/reader file-name))]
    (binding [*read-eval* false]
      (read r))))

(defn write-chnrs [canton community]
  (when-let [chnrs (get-chnrs canton community)]
    (data->file chnrs (data-file-name canton community))))

(defn read-chnrs [canton community]
  (file->data (data-file-name canton community)))

(defn valid-xml?
  "If given string is real xml return that, otherwise nil."
  [xml]
  (and xml (re-find #"^<\?xml version" xml) xml))

(defn get-xml
  "Get xml given a CH number.
Optionally accept additional request options as a hash-map."
  [chnr & options]
  (let [amt (re-find #"[^CH\-0]+?[^\.]*" chnr)
        options (first options)
        params (merge  {:socket-timeout 3000
                        :conn-timeout 800
                        :ignore-unknown-host? true}
                       options)
        response (:body (try
                          (client/get (format "http://zh.powernet.ch/webservices/net/hrg/hrg.asmx/getExcerpt?Chnr=%s&Amt=%s&Lang=1" chnr amt)
                                      params)
                          (catch Exception e (info
                                              (str "Exception"
                                                   (when options (format " %s " options))
                                                   ": " (.getMessage e))))))]
    (valid-xml? response)))

(def proxy-file "data/proxies.clj")

(def proxy-urls
  "A vector of proxy entries containing the URL and a possible function to retrieve the proxy data."
  [["http://98.126.61.179/yahoo/lists/check_report.txt"]
   ["http://www.blackhatworld.com/blackhat-seo/proxy-lists/453375-daily-proxies-all-proxy-protocols-17.html"
    #(-> (html/select (html/html-resource (java.net.URL. %)) #{[:.bbcode_code]}) last :content first)]])

(defn fetch-proxy-data [[url func]]
  (if func
    (func url)
    (slurp url)))

(defn parse-proxies [urls]
  (keep #(re-find #"[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+:[0-9]+" %)
        (reduce into
                (map #(clojure.string/split-lines (fetch-proxy-data %)) proxy-urls))))

(defn get-proxies []
  (parse-proxies proxy-urls))

(def proxies-pool (atom (get-proxies)))
(def proxies (atom @proxies-pool))

(defn add-proxies [url]
  (swap! proxies-pool #(seq (set/union (set (parse-proxies [url]))
                                      (set %)))))
(defn replace-proxies [url]
  (reset! proxies-pool (parse-proxies [url]))
  (reset! proxies @proxies-pool))

(defn get-proxy
  "Get the first proxy from `proxies'.
Return a hash-map with the form {:proxy-host \"8.8.8.8\" :proxy-port 8080}."
  [proxies]
  (let [[host port] (string/split (first proxies) #":")]
    {:proxy-host host :proxy-port (Integer. port)}))

(defn update-proxies [proxies remove?]
  (let [current-proxy (first proxies)
        proxies-rest (rest proxies)]
    (when remove?
      (swap! proxies-pool #(doall (remove #{current-proxy} %))))
    (if (empty? proxies-rest)
      (if (empty? @proxies-pool)
        (reset! proxies-pool (get-proxies))
        @proxies-pool)
      proxies-rest)))

(defn all-proxies->file
  "Persist all proxies to file.
If the file exists combine the lists (union)."
  []
  (let [file-name "data/proxies.clj"]
    (seq (set/union (when (.exists (io/as-file file-name))
                      (set (file->data file-name)))
                    (set @proxies-pool)))
    (data->file @proxies-pool file-name)))

(defn parse-xml [xml]
  (clojure.xml/parse (java.io.ByteArrayInputStream. (.getBytes xml))))

(defn write-xml [chnr]
  (let [file (format "data/chnrs/%s" chnr)
        xml (atom nil)]
    (if-not (.exists (io/as-file file))
      (do (while (not @xml)
            (reset! xml (get-xml chnr (get-proxy @proxies)))
            (swap! proxies #(update-proxies % (if @xml false true))))
          (when-let [data (try
                            (parse-xml @xml)
                            (catch Exception e
                              (data->file @xml (format "data/chnrs/failed/%s" chnr) :pretty true)
                              (info (format "Exception at %s: %s" chnr (.getMessage e)))))]
            (data->file data file :pretty true)
            (info (format "Wrote %s" file))))
      (info (format "Skipping %s because it already exists" file)))))

(defn scrape-chnrs [chnrs]
  (let [count (count chnrs)
        cursor (atom 0)]
    (doseq [chnr chnrs]
      (write-xml chnr)
      (swap! cursor inc)
      (info (format "%s/%s" @cursor count)))))

(defmacro scrape-job [jobname chnrs]
  `(def ~jobname (future (scrape-chnrs ~chnrs))))
